package com.mintic.crmapp.view.products;

public class AddListInfo {

    private String[] list;

    public String[] getList() {
        return list;
    }

    public void setList(String[] list) {
        this.list = list;
    }

    public AddListInfo(String[] list) {
        this.list = list;
    }
}
