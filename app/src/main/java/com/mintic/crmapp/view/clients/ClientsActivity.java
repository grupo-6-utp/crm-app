package com.mintic.crmapp.view.clients;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mintic.crmapp.R;
import com.mintic.crmapp.view.ListInfo;
import com.mintic.crmapp.view.orders.OrdersActivity;
import com.mintic.crmapp.view.products.ProductsActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ClientsActivity extends AppCompatActivity implements ClientsMVP.View{


    private ClientsMVP.Presenter presenter;

    TextView tvTitleClients;
    RecyclerView rvClients;
    FloatingActionButton btnAddClient;
    TextInputLayout tilNit;
    TextInputEditText etNit;
    TextInputLayout tilName;
    TextInputEditText etName;
    TextInputLayout tilDirection;
    TextInputEditText etDirection;
    TextInputLayout tilCel;
    TextInputEditText etCel;
    Button btnAddClient2;
    List<ListInfo> listClients;
    ImageButton btncerrarsesion;
    ImageButton btnorders;
    ImageButton btnclients;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clients);
        setUI();
    }

    private void setUI() {

        presenter = new ClientsPresenter(this);

        this.listClients = new ArrayList<>();

        rvClients = findViewById(R.id.rv_addproducts);
        tvTitleClients = findViewById(R.id.tv_titleproducts);
        btnAddClient = findViewById(R.id.btn_addProductActivity);

        btnAddClient2 = findViewById(R.id.btn_addproduct);

        tilNit=findViewById(R.id.til_addproduct);
        tilName=findViewById(R.id.til_nameproduct);
        tilDirection=findViewById(R.id.til_priceuni);
        tilCel=findViewById(R.id.til_cargarimagen);

        etNit=findViewById(R.id.et_addproduct);
        etName=findViewById(R.id.et_nameproduct);
        etDirection=findViewById(R.id.et_priceuni);
        etCel=findViewById(R.id.et_cel);

        tilNit.setVisibility(View.GONE);
        tilName.setVisibility(View.GONE);
        tilDirection.setVisibility(View.GONE);
        tilCel.setVisibility(View.GONE);
        btnAddClient2.setVisibility(View.GONE);

        btncerrarsesion = findViewById(R.id.btn_menucerrarsesion1);

        btnorders = findViewById(R.id.btn_menuorders1);

        btncerrarsesion.setOnClickListener(v -> {
            presenter.cerrarsesion();
        });

        btnorders.setOnClickListener(v -> {
            showActivity(OrdersActivity.class);
        });

        rvClients.setLayoutManager(new LinearLayoutManager(this));

        btnAddClient.setOnClickListener(v -> {
            //presenter.newClient();
            rvClients.setVisibility(View.GONE);
            btnAddClient.setVisibility(View.GONE);
            tvTitleClients.setText("Agregar Cliente");

            tilNit.setVisibility(View.VISIBLE);
            tilName.setVisibility(View.VISIBLE);
            tilDirection.setVisibility(View.VISIBLE);
            tilCel.setVisibility(View.VISIBLE);
            btnAddClient2.setVisibility(View.VISIBLE);

        });

        btnAddClient2.setOnClickListener(v -> presenter.newClient());

        presenter.loadClients();

    }


    @Override
    public ClientsInfo getAddClientsInfo() {
        return new ClientsInfo(
                0,
                etName.getText().toString().trim(),
                etDirection.getText().toString().trim(),
                etCel.getText().toString().trim(),
                etNit.getText().toString().trim()
        );
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        //Snackbar.make(rvOrders, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void showActivity(Class<? extends AppCompatActivity> type) {
        Intent intent = new Intent(this, type);
        startActivity(intent);
    }

    @Override
    public void showActivity(Class<ProductsActivity> type, Map<String, String> parametros) {
        Intent intent = new Intent(this, type);
        if (parametros != null) {
            for (String key : parametros.keySet()) {
                intent.putExtra(key, parametros.get(key));
            }
        }
        startActivity(intent);
    }

    @Override
    public void loadClients(List<ClientsInfo> clients) {
        this.listClients = new ArrayList<>();
        for (ClientsInfo client : clients){
            this.listClients.add(new ListInfo(client.getName()));
        }
        ClientsAdapter adapter = new ClientsAdapter(clients);
        adapter.setOnItemClickListener(info -> {
            showMap(info.getDirection());
        });
        rvClients.setAdapter(adapter);
    }

    @Override
    public void hideAddClient() {
        tvTitleClients.setText("Lista de Clientes");
        tilNit.setVisibility(View.GONE);
        tilName.setVisibility(View.GONE);
        tilDirection.setVisibility(View.GONE);
        //tilCel.setVisibility(View.GONE);
        //btnAddClient2.setVisibility(View.GONE);
        presenter.loadClients();
        rvClients.setVisibility(View.VISIBLE);
        btnAddClient.setVisibility(View.VISIBLE);
        etNit.setText("");
        etName.setText("");
        etCel.setText("");
        etDirection.setText("");
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(btnAddClient2.getWindowToken(), 0);
        tilCel.setVisibility(View.GONE);
        btnAddClient2.setVisibility(View.GONE);
    }

    @Override
    public void showErrorNit(String message) {
        tilNit.setError(message);
    }

    @Override
    public List<ListInfo> getClientsInfo() {
        presenter.loadClients();
        return this.listClients;
    }

    @Override
    public void showNameError(String message) {
        tilName.setError(message);
    }

    @Override
    public void showCelError(String message) {
        tilCel.setError(message);
    }

    @Override
    public void showDirectionError(String message) {
        tilDirection.setError(message);
    }

    public void showMap (String adress) {
        String url = "http://maps.google.com/maps?q="+adress; Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(url)); startActivity(intent);
    }

}