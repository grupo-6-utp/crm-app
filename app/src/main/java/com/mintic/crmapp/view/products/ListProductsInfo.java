package com.mintic.crmapp.view.products;

public class ListProductsInfo {

    private String product;
    private Integer unid;
    private Integer preciounit;
    private Integer total;

    public ListProductsInfo(String product, Integer unid, Integer preciounit, Integer total) {
        this.product = product;
        this.unid = unid;
        this.preciounit = preciounit;
        this.total = total;
    }

    public String getProduct() {
        return product;
    }

    public Integer getUnid() {
        return unid;
    }

    public Integer getPreciounit() {
        return preciounit;
    }

    public Integer getTotal() {
        return total;
    }
}
