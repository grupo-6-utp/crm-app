package com.mintic.crmapp.view.products;

public class AddProductsInfo {

    private String product;
    private String quantity;

    public AddProductsInfo(String product, String quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    public String getProduct() {
        return product;
    }

    public String getQuantity() {
        return quantity;
    }
}
