package com.mintic.crmapp.view;

import java.util.List;

public class ValidateTextList2 {

    private boolean validate;

    public ValidateTextList2(String text, List<ListInfo> list) {
        validate = false;
        for (ListInfo listInfo : list) {
            if (text.equals(listInfo.getString1())) {
                validate = true;
                break;
            }
        }
    }
    public boolean getValidate() {
        return validate;
    }

    public void setValidate(boolean validate) {
        this.validate = validate;
    }
}
