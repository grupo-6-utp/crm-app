package com.mintic.crmapp.view.orders;

import com.mintic.crmapp.model.repository.OrdersRepository;
import com.mintic.crmapp.view.clients.ClientsActivity;
import com.mintic.crmapp.view.login.MainActivity;
import com.mintic.crmapp.view.products.ListProductsInfo;
import com.mintic.crmapp.view.products.ProductsActivity;

import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

public class OrdersPresenter implements OrdersMVP.Presenter {

    private final OrdersMVP.View view;
    private final OrdersMVP.Model model;
    List<OrderInfo> ordersShare;
    List<ListProductsInfo> productsShare;


    public OrdersPresenter(OrdersMVP.View view) {

        this.view = view;
        this.model = new OrdersRepository(view.getApplicationContext(), this);
    }

    @Override
    public void loadOrders() {
        model.loadOrders();
    }

    @Override
    public void loadOrders(List<OrderInfo> orders) {
        view.loadOrders(orders);
    }

    @Override
    public void newOrder() {
        view.showActivity(ProductsActivity.class);
    }

    @Override
    public void cerrarsesion() {
        model.cerrarsesion();
        view.showActivity(MainActivity.class);
    }

    @Override
    public void loadTotalOrders(Integer totalorders) {
        view.showTotalOrders(totalorders.toString());
    }

    @Override
    public void newOrder(OrderInfo info) {
        Map<String, String> parametros = new HashMap<>();
        parametros.put(ProductsActivity.EXTRA_TOTAL, info.getTotalorder());
        parametros.put(ProductsActivity.EXTRA_CLIENT, info.getClient());
        parametros.put(ProductsActivity.EXTRA_LISTPRODUCTS, info.getListproducts());
        parametros.put(ProductsActivity.EXTRA_UID, info.getUid().toString());

        view.showActivity(ProductsActivity.class, parametros);
    }

    public void loadProducts() {
        model.loadProducts();
    }

    @Override
    public void loadProducts(List<ListProductsInfo> products) {
        view.loadProducts(products);
    }

    @Override
    public void clients() {
        view.showActivity(ClientsActivity.class);
    }

    @Override
    public void setShared(List<OrderInfo> ordersShare) {
        model.setShared(ordersShare, true);
        model.loadOrders();
    }

}

