package com.mintic.crmapp.view.forgetpassword;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mintic.crmapp.R;
import com.mintic.crmapp.view.UserRegister.UserRegisterMVP;
import com.mintic.crmapp.view.login.MainActivity;

public class ForgetPassActivity extends AppCompatActivity implements ForgetPassMVP.View{

    private ForgetPassMVP.Presenter presenter;

    TextInputLayout tilemail;
    TextInputEditText etemail;
    Button btnResetPass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_olvido_contrasena);
        InitUI();
    }

    private void InitUI() {

        presenter = new ForgetPassPresenter(this);

        tilemail=findViewById(R.id.til_forgetemail);
        etemail=findViewById(R.id.et_forgetemail);
        btnResetPass=findViewById(R.id.btn_resetpass);

        btnResetPass.setOnClickListener(v -> {
            presenter.passwordReset();
        });
    }

    @Override
    public String getEmailInfo() {
        return new String(etemail.getText().toString().trim());
    }

    @Override
    public void showEmailError(String message) {
        tilemail.setError(message);
    }

    @Override
    public void showActivity(Class<? extends AppCompatActivity> type) {
        Intent intent = new Intent(this, type);
        startActivity(intent);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}