package com.mintic.crmapp.view.login;

import com.mintic.crmapp.model.repository.UserRepository;
import com.mintic.crmapp.view.orders.OrdersActivity;


public class MainPresenter implements MainMVP.Presenter {

    private final MainMVP.View view;

    private final MainMVP.Model model;

    public MainPresenter(MainMVP.View view) {
        this.view = view;
        this.model = new UserRepository();
        this.model.setMainPresenter(this);
    }

    @Override
    public void Login() {
        LoginInfo info = view.getLoginInfo();
        //Validar Datos

        if (info.getEmail() == null
                || info.getEmail().trim().isEmpty()
                || !info.getEmail().contains("@")
        ) {
            view.showEmailError("Correo electronico no valido");
            return;
        }

        if (info.getPassword() == null || info.getPassword().trim().isEmpty() || info.getPassword().trim().length() < 6) {
            view.showPasswordError("Contraseña no valida");
            return;
        }

        model.validateUsernamePassword(info.getEmail(), info.getPassword());
    }

    @Override
    public void authenticate() {

        if (model.isAuthenticated()) {
            view.showActivity(OrdersActivity.class);
        }
    }

    @Override
    public void authenticationSuccessful() {
        view.showActivity(OrdersActivity.class);
    }

    @Override
    public void authenticationFailure(String message) {
        view.showErrorMessage(message);
    }
}