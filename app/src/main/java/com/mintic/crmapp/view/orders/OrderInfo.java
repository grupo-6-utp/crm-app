package com.mintic.crmapp.view.orders;

public class OrderInfo {

    private String client;
    private String totalorder;
    private String listproducts;
    private Integer uid;
    private Boolean shared;

    public OrderInfo(String client, String totalorder, String listproducts, Integer uid, Boolean shared) {
        this.client = client;
        this.totalorder = totalorder;
        this.listproducts = listproducts;
        this.uid = uid;
        this.shared = shared;
    }

    public Integer getUid() {
        return uid;
    }

    public String getListproducts() {
        return listproducts;
    }


    public OrderInfo(String client, String totalorder) {
        this.client = client;
        this.totalorder = totalorder;
    }

    public String getClient() {
        return client;
    }

    public String getTotalorder() {
        return totalorder;
    }
}
