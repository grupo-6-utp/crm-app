package com.mintic.crmapp.view.clients;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;
import com.mintic.crmapp.R;
import com.mintic.crmapp.view.orders.OrderInfo;
import com.mintic.crmapp.view.orders.OrdersAdapter;

import java.util.List;

public class ClientsAdapter extends RecyclerView.Adapter<ClientsAdapter.ViewHolder>{

    private List<ClientsInfo> data;
    private static ClientsAdapter.OnItemClickListener onItemClickListener;

    public ClientsAdapter(List<ClientsInfo> data) {
        this.data = data;
    }

    public void setOnItemClickListener(ClientsAdapter.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public ClientsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_orders, parent, false);

        return new ClientsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ClientsAdapter.ViewHolder holder, int position) {

        holder.setInfo(data.get(position));
        //holder.getTvCliente().setText(data.get(position).getClient());
        //holder.getTvTotalOrder().setText(data.get(position).getTotalorder());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener{

        private MaterialTextView tvCliente;
        private MaterialTextView tvTotalOrder;

        private ClientsInfo info;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvCliente = itemView.findViewById(R.id.tv_product);
            tvTotalOrder = itemView.findViewById(R.id.tv_unid);

            if (onItemClickListener != null) {
                itemView.setOnClickListener(this);
            }

        }

        public void setInfo(ClientsInfo info) {
            this.info = info;

            tvCliente.setText(info.getName());
            tvTotalOrder.setText(info.getDirection());
        }

        public MaterialTextView getTvCliente() {
            return tvCliente;
        }

        public MaterialTextView getTvTotalOrder() {
            return tvTotalOrder;
        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(info);
            }
        }
    }
    public interface OnItemClickListener {
        void onItemClick(ClientsInfo info);
    }

}
