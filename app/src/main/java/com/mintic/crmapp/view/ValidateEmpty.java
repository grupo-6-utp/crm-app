package com.mintic.crmapp.view;

public class ValidateEmpty {

    private boolean validate;

    public ValidateEmpty(String text) {
        if (text == null
                || text.trim().isEmpty()){
            validate=false;
        } else {
            validate=true;
        }
    }

    public boolean getValidate() {
        return validate;
    }

    public void setValidate(boolean validate) {
        this.validate = validate;
    }
}
