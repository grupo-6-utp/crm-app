package com.mintic.crmapp.view.orders;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.mintic.crmapp.R;
import com.mintic.crmapp.view.products.ListProductsInfo;
import com.mintic.crmapp.view.products.ProductsActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class OrdersActivity extends AppCompatActivity implements OrdersMVP.View{

    private OrdersMVP.Presenter presenter;

    RecyclerView rvOrders;
    TextView tvTotal;
    FloatingActionButton btnAddOrderActivity;
    FloatingActionButton btnShare;
    TextView titleact;
    List<OrderInfo> ordersShare;
    List<ListProductsInfo> productsShare;
    StringBuilder txtShare;
    Boolean ask;
    ImageButton btncerrarsesion;
    ImageButton btnorders;
    ImageButton btnclients;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);
        initUI();
    }

    private void initUI(){

        presenter = new OrdersPresenter(this);

        this.ask= false;
        this.txtShare= new StringBuilder();
        this.ordersShare= new ArrayList<>();
        this.productsShare= new ArrayList<>();

        rvOrders = findViewById(R.id.rv_addproducts);
        tvTotal = findViewById(R.id.tv_totalorders);
        titleact = findViewById(R.id.tv_titleproducts);

        rvOrders.setLayoutManager(new LinearLayoutManager(this));

        btnAddOrderActivity = findViewById(R.id.btn_addProductActivity);

        btnAddOrderActivity.setOnClickListener(v -> presenter.newOrder());

        btncerrarsesion = findViewById(R.id.btn_menucerrarsesion);

        btnclients = findViewById(R.id.btn_menuclients);

        btnorders = findViewById(R.id.btn_menuorders);

        btnShare = findViewById(R.id.btn_share);

        btnclients.setOnClickListener(v -> {
            presenter.clients();
        });

        btncerrarsesion.setOnClickListener(v -> {
            presenter.cerrarsesion();
        });

        btnShare.setOnClickListener(v -> {
            //Principio del textShare
            txtShare.delete(0,txtShare.length());
            txtShare.append("Pedidos del dia");
            txtShare.append(System.getProperty("line.separator"));

            shareOrders();

        });

    }
    @Override
    protected void onResume(){
        super.onResume();
        if (ask) {
            AlertDialog.Builder askShared = new AlertDialog.Builder(OrdersActivity.this);
            askShared.setMessage("¿Desea archivar los pedidos compartidos?")
                    .setCancelable(false)
                    .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (ordersShare!=null) {
                                presenter.setShared(ordersShare);
                            }
                        }
                    })
                    .setNegativeButton("No", null);
            AlertDialog titulo = askShared.create();
            titulo.setTitle("CRM APP by Luis Castillo");
            titulo.show();
            ask=false;
        }
        presenter.loadOrders();
        presenter.loadProducts();

    }
    @Override
    public void loadOrders(List<OrderInfo> orders) {
        ordersShare=orders;
        OrdersAdapter adapter = new OrdersAdapter(orders);
        adapter.setOnItemClickListener(info -> {
        presenter.newOrder(info);
        });
        rvOrders.setAdapter(adapter);
    }

    @Override
    public void loadProducts(List<ListProductsInfo> products) {
        productsShare=products;
    }

    @Override
    public void showTotalOrders(String value) {
        tvTotal.setText("$ "+value);
    }

    @Override
    public void showActivity(Class<? extends AppCompatActivity> type) {
        Intent intent = new Intent(this, type);
        startActivity(intent);
    }

    @Override
    public void showActivity(Class<ProductsActivity> type, Map<String, String> parametros ){
        Intent intent = new Intent(this, type);
        if (parametros != null) {
            for (String key : parametros.keySet()) {
                intent.putExtra(key, parametros.get(key));
            }
        }
        startActivity(intent);
    }

    @Override
    public void shareOrders() {
        for (OrderInfo order : ordersShare) {
            txtShare.append(System.getProperty("line.separator"));
            txtShare.append("*Cliente: "+order.getClient()+"*");
            txtShare.append(System.getProperty("line.separator"));
            txtShare.append("Productos: ");
            txtShare.append(System.getProperty("line.separator"));
            for (ListProductsInfo product:productsShare){
                if (order.getListproducts().equals(product.getPreciounit().toString())){
                    txtShare.append(product.getProduct()+": "+product.getUnid().toString()+" unid");
                    txtShare.append(System.getProperty("line.separator"));
                }
            }
        }
        sharedIntent(txtShare.toString());
        ask=true;

    }

    @Override
    public void sharedIntent(String toString) {
      /*  Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, toString);
        sendIntent.setType("text/plain");

        Intent shareIntent = Intent.createChooser(sendIntent, "CRMM APP");
        startActivity(shareIntent);*/
/////////////////
        /*
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.setPackage("com.whatsapp");
        intent.putExtra(Intent.EXTRA_TEXT, toString);

        try {
            startActivity(intent);
        } catch (android.content.ActivityNotFoundException ex) {
            ex.printStackTrace();

            Snackbar.make(rvOrders, "El dispositivo no tiene instalado WhatsApp", Snackbar.LENGTH_LONG)
                    .show();
        }
*/
        String phone = "+573134190580";
        String message = toString;
        try {
        startActivity(
                new Intent(Intent.ACTION_VIEW,
                        Uri.parse(
                                String.format("https://api.whatsapp.com/send?phone=%s&text=%s", phone, toString)
                        )
                )
        );}catch (android.content.ActivityNotFoundException ex) {
            ex.printStackTrace();

            Snackbar.make(rvOrders, "El dispositivo no tiene instalado WhatsApp", Snackbar.LENGTH_LONG)
                    .show();
        }






        // openWhatsappContact("+573134190580");
    }

    @Override
    public void showSuccessMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        //Snackbar.make(rvOrders, message, Snackbar.LENGTH_SHORT).show();
    }

}