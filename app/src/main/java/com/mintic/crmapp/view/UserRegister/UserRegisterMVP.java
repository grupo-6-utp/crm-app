package com.mintic.crmapp.view.UserRegister;

import androidx.appcompat.app.AppCompatActivity;

public interface UserRegisterMVP {

    interface Model {

        void setUserRegisterPresenter(Presenter presenter);

        void AddUser(String email, String password);

        void cerrarsesion();

    }

    interface Presenter {

        void AddUser();

        void AddUserSuccessful();

        void AddUserFailure(String message);

    }

    interface View {

        RegisterInfo getRegisterInfo();

        void showActivity(Class<? extends AppCompatActivity> type);

        void showEmaildError(String message);

        void showNameError(String message);

        void showPasswordError(String message);

        void showRPasswordError(String message);

        void showToastMessage(String message);
    }


}
