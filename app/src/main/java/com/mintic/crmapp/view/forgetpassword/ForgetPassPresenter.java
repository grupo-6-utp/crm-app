package com.mintic.crmapp.view.forgetpassword;

import com.mintic.crmapp.model.repository.ForgetPassRepository;
import com.mintic.crmapp.view.login.MainActivity;

public class ForgetPassPresenter implements ForgetPassMVP.Presenter{

    private final ForgetPassMVP.View view;
    private final ForgetPassMVP.Model model;

    public ForgetPassPresenter(ForgetPassMVP.View view) {
        this.view = view;
        this.model = new ForgetPassRepository();
        this.model.setPresenter(this);
    }


    @Override
    public void passwordReset() {
        String email;
        email = view.getEmailInfo();

        if (email == null
                || email.trim().isEmpty()
                || !email.contains("@")
        ) {
            view.showEmailError("Correo electronico no valido");
            return;
        }

        model.passwordReset(email);

    }

    @Override
    public void passResetOK() {
        view.showToastMessage("Se envió un correo para la recuperacion de la contraseña");
        view.showActivity(MainActivity.class);
    }

    @Override
    public void passResetFailure(String message) {
        view.showToastMessage(message);
    }
}
