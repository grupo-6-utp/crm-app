package com.mintic.crmapp.view;

import java.lang.reflect.Array;

public class ValidateTextList {

    private boolean validate;

    public ValidateTextList(String text, String[] list) {
        validate = false;
        for (String i : list) {
            if (text.equals(i)) {
                validate = true;
                break;
            }
        }
    }
    public boolean getValidate() {
        return validate;
    }

    public void setValidate(boolean validate) {
        this.validate = validate;
    }
}
