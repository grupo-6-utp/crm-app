package com.mintic.crmapp.view.UserRegister;

import com.mintic.crmapp.model.repository.ProductsRepository;
import com.mintic.crmapp.model.repository.UserRegisterRepository;
import com.mintic.crmapp.view.ValidateEmail;
import com.mintic.crmapp.view.ValidateEmpty;
import com.mintic.crmapp.view.login.MainActivity;

public class UserRegisterPresenter implements UserRegisterMVP.Presenter{

    private final UserRegisterMVP.View view;

    private final UserRegisterMVP.Model model;

    public UserRegisterPresenter(UserRegisterMVP.View view) {
        this.view = view;
        this.model = new UserRegisterRepository();
        this.model.setUserRegisterPresenter(this);
    }


    @Override
    public void AddUser() {

        RegisterInfo info = view.getRegisterInfo();

        ValidateEmail valEmail = new ValidateEmail(info.getEmail().toString());

        ValidateEmpty valName = new ValidateEmpty(info.getName().toString());

        ValidateEmpty valPassword = new ValidateEmpty(info.getPassword().toString());

        ValidateEmpty valrPassword = new ValidateEmpty(info.getRPassword().toString());

        Boolean val2Password;

        if ((info.getPassword().equals(info.getRPassword()))
        ){
            val2Password = true;
        }else{
            val2Password = false;
        }

        if ((info.getPassword().length()<6)
        ){
            valPassword.setValidate(false);
        }else{

        }

        if ((valEmail.getValidate())
                && (valName.getValidate())
                && (valPassword.getValidate())
                && (valrPassword.getValidate())
                && (val2Password)
        ){
            model.AddUser(info.getEmail(),info.getPassword());
        }else{
            if (!val2Password){
                view.showRPasswordError("Las contraseñas no coinciden");
            }
            if (!valPassword.getValidate()){
                view.showPasswordError("Contraseña no valida");
            }
      //      if (!valrPassword.getValidate()){
      //          view.showRPasswordError("Contraseña no valida");
        //    }
            if (!valName.getValidate()){
                view.showNameError("Nombre no valido");
            }
            if (!valEmail.getValidate()){
                view.showEmaildError("Correo no valido");
            }
        }


    }

    @Override
    public void AddUserSuccessful() {
        view.showToastMessage("Se ha creado el usuario exitosamente");
        model.cerrarsesion();
        view.showActivity(MainActivity.class);
    }

    @Override
    public void AddUserFailure(String message) {
        view.showToastMessage(message);
    }


}
