package com.mintic.crmapp.view.login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mintic.crmapp.R;
import com.mintic.crmapp.view.UserRegister.UserRegisterActivity;
import com.mintic.crmapp.view.forgetpassword.ForgetPassActivity;

public class MainActivity extends AppCompatActivity implements MainMVP.View {

    private MainMVP.Presenter presenter;

    TextInputLayout tilUsername;
    TextInputEditText etUsername;
    TextInputLayout tilPassword;
    TextInputEditText etPassword;
    Button btnLogin;
    TextView tvRegister;
    TextView tvForgetPassword;

    private void initUI() {

        presenter=new MainPresenter(this);

        tilUsername = findViewById(R.id.til_username);
        etUsername = findViewById(R.id.et_username);
        tilPassword = findViewById(R.id.til_password);
        etPassword = findViewById(R.id.et_password);

        btnLogin = findViewById(R.id.btn_login);

        tvRegister = findViewById(R.id.tv_register);
        tvForgetPassword = findViewById(R.id.tv_totalorder);



        btnLogin.setOnClickListener(v -> {
            tilUsername.setError("");
            tilPassword.setError("");
            presenter.Login();
        });

        tvRegister.setOnClickListener(v -> {
            showActivity(UserRegisterActivity.class);
        });

        tvForgetPassword.setOnClickListener(v -> {showActivity(ForgetPassActivity.class);});

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.authenticate();
    }



    @Override
    public LoginInfo getLoginInfo() {
        return new LoginInfo(
                etUsername.getText().toString().trim(),
                etPassword.getText().toString().trim());
    }

    @Override
    public void showEmailError(String message) {tilUsername.setError(message);}

    @Override
    public void showPasswordError(String message) {
        tilPassword.setError(message);
    }

    @Override
    public void showActivity(Class<? extends AppCompatActivity> type) {
        Intent intent = new Intent(this, type);
        startActivity(intent);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showErrorMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}