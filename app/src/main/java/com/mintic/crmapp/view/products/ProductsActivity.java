package com.mintic.crmapp.view.products;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mintic.crmapp.R;
import com.mintic.crmapp.view.ListInfo;

import java.util.ArrayList;
import java.util.List;

public class ProductsActivity extends AppCompatActivity implements ProductsMVP.View {

    public static final String EXTRA_TOTAL="total";
    public static final String EXTRA_CLIENT="client";
    public static final String EXTRA_LISTPRODUCTS="listproducts";
    public static final String EXTRA_UID="uid";


    private ProductsMVP.Presenter presenter;

    TextInputLayout tilClient;
    TextInputLayout tilProduct;
    TextInputLayout tilQuantity;
    TextInputEditText etQuantity;
    Button btnAddProduct;
    Button btnSaveOrder;
    RecyclerView rvProducts;
    TextView tvTotalOrder;
    AppCompatAutoCompleteTextView spProducts;
    AppCompatAutoCompleteTextView spClients;
    TextView tvTitle;
    List<ListInfo> listClients;
    String[] listProducts;
    String[] listClients1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_order);
        InitUI();
    }

    private void InitUI (){

        presenter=new ProductsPresenter(this);

        this.listClients = new ArrayList<>();

        this.listProducts= new String[1];

        this.listClients1= new String[1];

        tilClient = findViewById(R.id.til_client);
        tilProduct = findViewById(R.id.til_product);
        tilQuantity = findViewById(R.id.til_quantity);
        etQuantity = findViewById(R.id.et_quantity);

        rvProducts = findViewById(R.id.rv_products);
        rvProducts.setLayoutManager(new LinearLayoutManager(this));

        tvTotalOrder = findViewById(R.id.tv_unid);
        tvTitle = findViewById(R.id.tv_titleaddorder);

        spProducts=findViewById(R.id.sp_products);

        spClients = findViewById(R.id.sp_clients);

        btnAddProduct = findViewById(R.id.btn_addProduct);
        btnAddProduct.setOnClickListener(v -> {

                    presenter.addProduct();
                    presenter.loadProducts();
                    if ((tilProduct.getError()==null)&&(tilQuantity.getError()==null)){
                        spProducts.setText("");
                        etQuantity.setText("");
                        spProducts.requestFocus();
                    }
                });

        btnSaveOrder = findViewById(R.id.btn_saveOrder);
        btnSaveOrder.setOnClickListener(v -> {
            presenter.saveOrder();
        });


        if (getIntent().getStringExtra(EXTRA_CLIENT) == null) {presenter.loadProducts();}
        else{
            tilClient.setEnabled(false);

            presenter.loadProducts(getIntent().getStringExtra(EXTRA_LISTPRODUCTS));
            tilProduct.setEnabled(false);

            tvTotalOrder.setText(getIntent().getStringExtra(EXTRA_TOTAL));

            tvTitle.setText("Pedido de "+getIntent().getStringExtra(EXTRA_CLIENT));
            tilClient.setVisibility(View.GONE);
            tilProduct.setVisibility(View.GONE);
            tilQuantity.setVisibility(View.GONE);
            btnAddProduct.setVisibility(View.GONE);
            btnSaveOrder.setEnabled(false);


        }

        presenter.loadClients();
        presenter.loadToTextProducts();


    }

    public void onBackPressed() {
        finish();
    }


    protected void onResume(){
        super.onResume();
        presenter.loadProducts();
    }

 /*   @Override
    public void onSaveInstanceState(@NonNull Bundle outState){
        super.onSaveInstanceState(outState);
        outState.putString("client", spClients.getText().toString());
        outState.putString("listproduct", spProducts  .getText().toString());
        outState.putString("total", tvTotalOrder.getText().toString());
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        spClients.setText(savedInstanceState.getString("client"));
        spProducts.setText(savedInstanceState.getString("listproduct"));
        tvTotalOrder.setText(savedInstanceState.getString("total"));
    }
*/
    @Override
    public AddProductsInfo getAddProductsInfo() {
        return new AddProductsInfo(
                spProducts.getText().toString().trim(),
                etQuantity.getText().toString().trim()
        );
    }

    public AddOrderInfo getAddOrderInfo(){
        Integer list = presenter.getListProducts();
        return new AddOrderInfo (
                spClients.getText().toString().trim(),
                tvTotalOrder.getText().toString().trim(),
                list.toString(),
                "FALSO"
        );
    }

    @Override
    public void getAddClientsListInfo(List<ListInfo> response) {
        this.listClients=response;
    }

    @Override
    public String[] getAddClientsListInfo() {
        return this.listClients1;
    }

    @Override
    public String[] getAddProductsListInfo() {
        return this.listProducts;
    }

    public void loadProducts(List<ListProductsInfo> products) {
        rvProducts.setAdapter(new ProductsAdapter(products));
    }

    @Override
    public void showErrorClient(String message) {
        tilClient.setError(message);
    }

    @Override
    public void showErrorProduct(String message) {
        tilProduct.setError(message);
    }

    @Override
    public void showErrorQuantity(String message) {
        tilQuantity.setError(message);
    }

    @Override
    public void showSuccessMessage(String message) {

        //Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        Snackbar.make(tvTotalOrder,message,Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void closeActivity() {
        finish();
    }

    @Override
    public void showTotalValue(String value) {
        tvTotalOrder.setText(value);
    }

    @Override
    public void loadClients(String [] response) {
        this.listClients1= new String[response.length];
        this.listClients1= response;
        spClients.setAdapter(new ArrayAdapter<String>(ProductsActivity.this,R.layout.item_listproducts,response));
    }

    @Override
    public void loadToTextProducts(String[] response) {
        this.listProducts= new String[response.length];
        this.listProducts= response;
        spProducts.setAdapter(new ArrayAdapter<String>(ProductsActivity.this,R.layout.item_listproducts,response));
    }
}