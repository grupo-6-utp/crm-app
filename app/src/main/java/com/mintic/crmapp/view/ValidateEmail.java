package com.mintic.crmapp.view;

public class ValidateEmail {

    private Boolean validate;

    public ValidateEmail(String email) {
        if (email == null
                || email.trim().isEmpty()
                || !email.contains("@")){
            validate=false;
        } else {
            validate=true;
        }
    }

    public Boolean getValidate() {
        return validate;
    }
/*
    public ValidateEmail(String toString) {

    }

    public boolean ValidateEmail(String email) {
        if (email == null
                || email.trim().isEmpty()
                || !email.contains("@")){
            return false;
        } else {
            return true;
        }
    }

   /* public Boolean ValidateEmail(String email) {

        if (email == null
                || email.trim().isEmpty()
                || !email.contains("@")){
            return false;
        } else {
            return true;
        }
    }*/
}
