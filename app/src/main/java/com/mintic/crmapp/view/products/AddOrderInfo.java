package com.mintic.crmapp.view.products;

public class AddOrderInfo {

    private String client;
    private String totalorder;
    private String listproduct;
    private String shared;


    public AddOrderInfo(String client, String totalorder, String listproduct, String shared) {
        this.client = client;
        this.totalorder = totalorder;
        this.listproduct = listproduct;
        this.shared = shared;
    }

    public String getClient() {
        return client;
    }

    public String getTotalorder() {
        return totalorder;
    }

    public String getListproduct() {
        return listproduct;
    }

    public String getShared() {
        return shared;
    }

}
