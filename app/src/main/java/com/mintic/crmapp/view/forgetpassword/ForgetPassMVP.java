package com.mintic.crmapp.view.forgetpassword;

import androidx.appcompat.app.AppCompatActivity;

public interface ForgetPassMVP {

    interface Model {

        void passwordReset(String email);
        void setPresenter(Presenter presenter);
    }
    interface Presenter {

        void passwordReset ();
        void passResetOK();
        void passResetFailure(String message);

    }
    interface View {

        String getEmailInfo();
        void showEmailError(String message);
        void showActivity(Class<? extends AppCompatActivity> type);
        void showToastMessage(String message);

    }
}
