package com.mintic.crmapp.view.clients;

public class ClientsInfo {

    private Integer uid;

    private String name;

    private String direction;

    private String cel;

    private String nit;

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getCel() {
        return cel;
    }

    public void setCel(String cel) {
        this.cel = cel;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public ClientsInfo(Integer uid, String name, String direction, String cel, String nit) {
        this.uid = uid;
        this.name = name;
        this.direction = direction;
        this.cel = cel;
        this.nit = nit;
    }
}
