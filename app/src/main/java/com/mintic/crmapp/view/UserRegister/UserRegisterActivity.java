package com.mintic.crmapp.view.UserRegister;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mintic.crmapp.R;
import com.mintic.crmapp.view.login.MainActivity;

public class UserRegisterActivity extends AppCompatActivity  implements UserRegisterMVP.View {

    private UserRegisterMVP.Presenter presenter;

    TextInputLayout tilName;
    TextInputEditText etName;
    TextInputLayout tilEmail;
    TextInputEditText etEmail;
    TextInputLayout tilPassword;
    TextInputEditText etPassword;
    TextInputLayout tilRtPassword;
    TextInputEditText etRtPassword;
    Button btnRegister;
    TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_register);
        InitUI();
    }

    private void InitUI() {

        presenter = new UserRegisterPresenter(this);

        tilName= findViewById(R.id.til_name);
        etName= findViewById(R.id.et_name);
        tilEmail= findViewById(R.id.til_email);
        etEmail= findViewById(R.id.et_email);
        tilPassword= findViewById(R.id.til_aupassword);
        etPassword= findViewById(R.id.et_aupassword);
        tilRtPassword= findViewById(R.id.til_aurepeatpass);
        etRtPassword= findViewById(R.id.et_aurepeatpass);
        textView= findViewById(R.id.textView5);

        btnRegister = findViewById(R.id.btn_register);

        btnRegister.setOnClickListener(v -> {
            tilEmail.setError("");
            tilName.setError("");
            tilPassword.setError("");
            tilRtPassword.setError("");
            presenter.AddUser();
        });

        textView.setOnClickListener(v -> showActivity(MainActivity.class));
    }

    @Override
    public RegisterInfo getRegisterInfo() {
        return new RegisterInfo(
                etEmail.getText().toString().trim(),
                etName.getText().toString().trim(),
                etPassword.getText().toString().trim(),
                etRtPassword.getText().toString().trim());
    }

    @Override
    public void showActivity(Class<? extends AppCompatActivity> type) {
        Intent intent = new Intent(this, type);
        startActivity(intent);
    }

    @Override
    public void showEmaildError(String message) {
        tilEmail.setError(message);
    }

    @Override
    public void showNameError(String message) {
        tilName.setError(message);
    }

    @Override
    public void showPasswordError(String message) {
        tilPassword.setError(message);
    }

    @Override
    public void showRPasswordError(String message) {
        tilRtPassword.setError(message);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

}