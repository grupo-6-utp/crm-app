package com.mintic.crmapp.view.login;

import androidx.appcompat.app.AppCompatActivity;

import java.security.Principal;

public interface MainMVP {

    interface Presenter {

        void Login();

        void authenticate();

        void authenticationSuccessful();

        void authenticationFailure(String message);

    }

    interface View {

        LoginInfo getLoginInfo();

        void showEmailError(String message);

        void showPasswordError(String message);

        void showActivity(Class<? extends AppCompatActivity> type);

        void showToastMessage(String message);

        void showErrorMessage(String message);

    }

    interface Model {

        void validateUsernamePassword(String email, String password);

        void setMainPresenter(Presenter presenter);

        boolean isAuthenticated();
    }

}
