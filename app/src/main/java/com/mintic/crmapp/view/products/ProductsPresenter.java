package com.mintic.crmapp.view.products;

import com.mintic.crmapp.model.repository.ProductsRepository;
import com.mintic.crmapp.view.ListInfo;
import com.mintic.crmapp.view.ValidateEmpty;
import com.mintic.crmapp.view.ValidateTextList;
import com.mintic.crmapp.view.ValidateTextList2;

import java.util.Arrays;
import java.util.List;

public class ProductsPresenter implements ProductsMVP.Presenter {

private final ProductsMVP.View view;
private final ProductsMVP.Model model;

    public ProductsPresenter(ProductsMVP.View view) {

        this.view = view;
        this.model=new ProductsRepository(view.getApplicationContext(),this);
    }

    @Override
    public void saveOrder() {

        AddOrderInfo info = view.getAddOrderInfo();

        view.showErrorClient("");
        view.showErrorProduct("");


        ValidateEmpty valClient = new ValidateEmpty(info.getClient());
        Boolean valTotal;
        if(!(info.getTotalorder().equals("0"))) {
            valTotal = true;
        }else{valTotal=false;}
        String [] clients=view.getAddClientsListInfo();
        ValidateTextList valClientExist= new ValidateTextList(info.getClient(),clients);

        if (valClient.getValidate()){
            if (valTotal){
                if (valClientExist.getValidate()){
                    model.saveNewOrder(info);
                    view.showSuccessMessage("Pedido Guardado Exitosamente");
                    view.closeActivity();
                }else{view.showErrorClient("El cliente no existe, ingrese un cliente valido");}
            }else{view.showErrorProduct("No ha ingresado un producto");}
        }else{view.showErrorClient("Ingrese un cliente");}

    }

    @Override
    public void addProduct() {

        view.showErrorProduct("");
        view.showErrorQuantity("");

        AddProductsInfo info=view.getAddProductsInfo();

        ValidateEmpty valProduct = new ValidateEmpty(info.getProduct());
        ValidateEmpty valQuantity = new ValidateEmpty(info.getQuantity());
        String[] products=view.getAddProductsListInfo();
        ValidateTextList valProductExist= new ValidateTextList(info.getProduct(),products);

        if (valProduct.getValidate()){
            if (valQuantity.getValidate()&& !(Integer.parseInt(info.getQuantity()) ==0)){
                if (valProductExist.getValidate()){
                    model.saveNewProduct(info);
                }else{view.showErrorProduct("El producto no existe, ingrese un producto valido");}
            }else{view.showErrorQuantity("Ingrese la cantidad");}
        }else{view.showErrorProduct("Ingrese un producto");}


    }

    @Override
    public void loadProducts() { model.loadProducts(); }

    @Override
    public void loadProducts(List<ListProductsInfo> products) { view.loadProducts(products); }

    @Override
    public void loadTotal(Integer total) {
        view.showTotalValue(total.toString());
    }

    @Override
    public Integer getListProducts() {
        return model.getListProducts();
    }

    @Override
    public void loadProducts(String listproducts) {
        model.loadProducts(listproducts);
    }

    @Override
    public void loadClients() {
        model.loadClients();
    }

    @Override
    public void loadClients(String [] response) {
        view.loadClients(response);
    }

    @Override
    public void loadToTextProducts() {
        model.loadListProduct();
    }

    @Override
    public void loadToTextProducts(String[] name) {
        view.loadToTextProducts(name);
    }

}
