package com.mintic.crmapp.view.clients;

import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;

import com.mintic.crmapp.view.ListInfo;
import com.mintic.crmapp.view.products.ProductsActivity;

import java.util.List;
import java.util.Map;

public interface ClientsMVP {

    interface Model {

        void loadClients();

        void newClients(ClientsInfo info);

        void cerrarsesion();
    }

    interface Presenter {

        void loadClients();

        void loadClients(List<ClientsInfo> clients);

        void newClient();

        void cerrarsesion();
    }

    interface View {

        ClientsInfo getAddClientsInfo();

        void showMessage(String message);

        void showActivity(Class<? extends AppCompatActivity> type);

        void showActivity(Class<ProductsActivity> productsActivityClass, Map<String, String> parametros);

        Context getApplicationContext();

        void loadClients(List<ClientsInfo> clients);

        void hideAddClient();

        void showErrorNit(String message);

        List<ListInfo> getClientsInfo();

        void showNameError(String message);

        void showCelError(String message);

        void showDirectionError(String message);

    }
}
