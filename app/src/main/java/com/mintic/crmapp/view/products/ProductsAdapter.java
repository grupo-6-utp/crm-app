package com.mintic.crmapp.view.products;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;
import com.mintic.crmapp.R;

import java.util.List;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolder> {

    List<ListProductsInfo> data;

    public ProductsAdapter(List<ListProductsInfo> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_products, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductsAdapter.ViewHolder holder, int position) {


        holder.getTvProduct().setText(data.get(position).getProduct());
        holder.getTvUnid().setText(data.get(position).getUnid().toString()+"  Und");
        holder.getTvPrecioUnitario().setText("Precio Unitario: $"+data.get(position).getPreciounit().toString());
        holder.getTvTotalProd().setText("Total: $"+data.get(position).getTotal().toString()+"   ");

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        MaterialTextView tvProduct;
        MaterialTextView tvUnid;
        MaterialTextView tvPrecioUnitario;
        MaterialTextView tvTotalProd;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvProduct = itemView.findViewById(R.id.tv_product);
            tvUnid = itemView.findViewById(R.id.tv_unid);
            tvPrecioUnitario = itemView.findViewById(R.id.tv_preciounitario);
            tvTotalProd = itemView.findViewById(R.id.tv_totalprod);
        }

        public MaterialTextView getTvProduct() {
            return tvProduct;
        }

        public MaterialTextView getTvUnid() {
            return tvUnid;
        }

        public MaterialTextView getTvPrecioUnitario() {
            return tvPrecioUnitario;
        }

        public MaterialTextView getTvTotalProd() {
            return tvTotalProd;
        }

    }

}

