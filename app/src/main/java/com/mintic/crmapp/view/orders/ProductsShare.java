package com.mintic.crmapp.view.orders;

public class ProductsShare {

    private String productsShare;

    public ProductsShare(String productsShare) {
        this.productsShare = productsShare;
    }

    public String getProductsShare() {
        return productsShare;
    }

    public void setProductsShare(String productsShare) {
        this.productsShare = productsShare;
    }
}
