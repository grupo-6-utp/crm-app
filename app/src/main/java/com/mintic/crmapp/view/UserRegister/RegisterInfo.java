package com.mintic.crmapp.view.UserRegister;

public class RegisterInfo {

    private String email;
    private String name;
    private String password;
    private String rpassword;

    public RegisterInfo(String email, String name, String password, String rpassword) {
        this.email = email;
        this.name = name;
        this.password = password;
        this.rpassword = rpassword;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public String getRPassword() {
        return rpassword;
    }
}
