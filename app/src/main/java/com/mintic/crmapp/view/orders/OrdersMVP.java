package com.mintic.crmapp.view.orders;

import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;

import com.mintic.crmapp.view.products.ListProductsInfo;
import com.mintic.crmapp.view.products.ProductsActivity;

import java.util.List;
import java.util.Map;

public interface OrdersMVP {


    interface Model {

        void loadOrders();

        void cerrarsesion();

        void loadProducts();

        void setShared(List<OrderInfo> ordersShare, Boolean var);
    }

    interface Presenter {

        void loadOrders();

        void loadOrders(List<OrderInfo> orders);

        void loadProducts();

        void newOrder();

        void cerrarsesion();

        void loadTotalOrders(Integer totalorders);

        void newOrder(OrderInfo info);

        void loadProducts(List<ListProductsInfo> products);

        void clients();

        void setShared(List<OrderInfo> ordersShare);
    }

    interface View {

        Context getApplicationContext();

        void loadOrders(List<OrderInfo> orders);

        void loadProducts(List<ListProductsInfo> products);

        void showTotalOrders(String value);

        void showActivity(Class<? extends AppCompatActivity> type);

        void showActivity(Class<ProductsActivity> productsActivityClass, Map<String, String> parametros);

        void shareOrders();

        void sharedIntent(String toString);

        void showSuccessMessage(String message);

    }

}
