package com.mintic.crmapp.view.clients;

import com.mintic.crmapp.model.repository.ClientsRepository;
import com.mintic.crmapp.model.repository.OrdersRepository;
import com.mintic.crmapp.view.ListInfo;
import com.mintic.crmapp.view.ValidateEmpty;
import com.mintic.crmapp.view.ValidateTextList;
import com.mintic.crmapp.view.ValidateTextList2;
import com.mintic.crmapp.view.login.MainActivity;
import com.mintic.crmapp.view.orders.OrdersMVP;
import com.mintic.crmapp.view.products.AddListInfo;

import java.util.List;

public class ClientsPresenter implements ClientsMVP.Presenter{

    private final ClientsMVP.View view;
    private final ClientsMVP.Model model;


    public ClientsPresenter(ClientsMVP.View view) {
        this.view = view;
        this.model = new ClientsRepository(view.getApplicationContext(), this);
    }

    @Override
    public void loadClients() {
        model.loadClients();
    }

    @Override
    public void loadClients(List<ClientsInfo> clients) {
        view.loadClients(clients);
    }


    @Override
    public void newClient() {
        ClientsInfo info = view.getAddClientsInfo();
        view.showErrorNit("");
        view.showCelError("");
        view.showDirectionError("");
        view.showNameError("");

        //Validacion NIT
        ValidateEmpty valNit = new ValidateEmpty(info.getNit());

        if (!(valNit.getValidate())) {view.showErrorNit("Ingrese el nit");
        }else {
            try {
                Integer.parseInt(info.getNit());
                view.showErrorNit("");
            } catch(Exception e) {
                valNit.setValidate(false);
                view.showErrorNit("El nit debe ser numerico y sin guiones ej:1234567890");
            }

        }

        //Validacion name
        List<ListInfo> clients = view.getClientsInfo();
        ValidateEmpty valName = new ValidateEmpty(info.getName());
        ValidateTextList2 valNameExist = new ValidateTextList2(info.getName(),clients);
        if (!valName.getValidate()) {view.showNameError("Ingrese el cliente");
        }else {
            if (valNameExist.getValidate()){
                view.showNameError("El cliente ya existe");
            }
        }


        //Validacion direction
        ValidateEmpty valDirection = new ValidateEmpty(info.getDirection());
        if (!valDirection.getValidate()) {view.showDirectionError("Ingrese la direccion");}
        else{info.setDirection(info.getDirection().replace("#","no "));}

        //Validacion cel
        ValidateEmpty valCel = new ValidateEmpty(info.getCel());
        if (!valCel.getValidate()) {view.showCelError("Ingrese el telefono");}

        if ((valNit.getValidate()) && (valCel.getValidate()) && (valName.getValidate()) && (valDirection.getValidate()) && (!valNameExist.getValidate())){
            model.newClients(info);
            view.hideAddClient();
        }
    }

    @Override
    public void cerrarsesion() {
        model.cerrarsesion();
        view.showActivity(MainActivity.class);
    }
}
