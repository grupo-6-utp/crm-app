package com.mintic.crmapp.view.products;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;
import com.mintic.crmapp.R;
import com.mintic.crmapp.view.ListInfo;

import java.util.List;

public class ListInfoAdapter extends RecyclerView.Adapter<ListInfoAdapter.ViewHolder> {

    List<ListInfo> data;

    public ListInfoAdapter(List<ListInfo> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public ListInfoAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_listproducts, parent, false);

        return new ListInfoAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListInfoAdapter.ViewHolder holder, int position) {

        holder.getTvText().setText(data.get(position).getString1());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        MaterialTextView tvText;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvText = itemView.findViewById(R.id.textlist);
        }

        public MaterialTextView getTvText() {
            return tvText;
        }

    }

}

