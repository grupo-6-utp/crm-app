package com.mintic.crmapp.view.orders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;
import com.mintic.crmapp.R;
import com.mintic.crmapp.model.entity.Order;

import java.util.List;

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.ViewHolder> {

    private List<OrderInfo> data;
    private static OnItemClickListener onItemClickListener;

    public OrdersAdapter(List<OrderInfo> data) {
        this.data = data;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_orders, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrdersAdapter.ViewHolder holder, int position) {

        holder.setInfo(data.get(position));
        //holder.getTvCliente().setText(data.get(position).getClient());
        //holder.getTvTotalOrder().setText(data.get(position).getTotalorder());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener{

        private MaterialTextView tvCliente;
        private MaterialTextView tvTotalOrder;

        private OrderInfo info;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvCliente = itemView.findViewById(R.id.tv_product);
            tvTotalOrder = itemView.findViewById(R.id.tv_unid);

            if (onItemClickListener != null) {
                itemView.setOnClickListener(this);
            }

        }

        public void setInfo(OrderInfo info) {
            this.info = info;

            tvCliente.setText(info.getClient());
            tvTotalOrder.setText(info.getTotalorder());
        }

        public MaterialTextView getTvCliente() {
            return tvCliente;
        }

        public MaterialTextView getTvTotalOrder() {
            return tvTotalOrder;
        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClick(info);
            }
        }
    }
    public interface OnItemClickListener {
        void onItemClick(OrderInfo info);
    }

}
