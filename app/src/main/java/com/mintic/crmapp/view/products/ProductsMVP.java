package com.mintic.crmapp.view.products;

import android.content.Context;

import com.mintic.crmapp.view.ListInfo;

import java.util.List;

public interface ProductsMVP {

    interface Presenter {

        void saveOrder();
        void addProduct();
        void loadProducts();
        void loadProducts(List<ListProductsInfo> products);
        void loadTotal(Integer total);

        Integer getListProducts();

        void loadProducts(String listproducts);

        void loadClients();

        void loadClients(String [] response);

        void loadToTextProducts ();

        void loadToTextProducts (String [] name);
    }

    interface View {

        AddProductsInfo getAddProductsInfo();
        AddOrderInfo getAddOrderInfo();
        void getAddClientsListInfo(List<ListInfo> response);

        String[] getAddClientsListInfo();

        String[] getAddProductsListInfo();

        void loadProducts(List<ListProductsInfo> products);
        void showErrorClient(String message);
        void showErrorProduct(String message);
        void showErrorQuantity(String message);

        void showSuccessMessage(String message);
        void closeActivity();
        void showTotalValue(String value);

        Context getApplicationContext();

        void loadClients(String [] response);

        void loadToTextProducts(String[] name);

    }

    interface Model {

        //List<AddProductsInfo> loadProducts();
        void loadProducts();

        Integer getPrecioUnit(String product);

        void saveNewOrder(AddOrderInfo info);
        void saveNewProduct(AddProductsInfo info);

        Integer calculateTotal();

        Integer getListProducts();

        void loadProducts(String listproducts);

        void loadClients();

        void loadListProduct();
    }


}
