package com.mintic.crmapp.model.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Product {

    @PrimaryKey
    private Integer uid;

    private String name;

    private Integer preciounit;

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPreciounit() {
        return preciounit;
    }

    public void setPreciounit(Integer preciounit) {
        this.preciounit = preciounit;
    }

    @Override
    public String toString(){
        return "Products{" +
                "uid=" + uid +
                ", product='" + name + '\'' +
                ", preciounit='" + preciounit +
                '}';
    }

}
