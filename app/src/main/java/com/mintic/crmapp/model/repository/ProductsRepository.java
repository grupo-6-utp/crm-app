package com.mintic.crmapp.model.repository;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mintic.crmapp.model.entity.Client;
import com.mintic.crmapp.model.entity.ListProduct;
import com.mintic.crmapp.model.entity.Order;
import com.mintic.crmapp.model.entity.Product;
import com.mintic.crmapp.view.products.AddOrderInfo;
import com.mintic.crmapp.view.products.ListProductsInfo;
import com.mintic.crmapp.view.products.ProductsMVP;
import com.mintic.crmapp.view.products.ProductsPresenter;
import com.mintic.crmapp.view.products.AddProductsInfo;

import java.util.ArrayList;
import java.util.List;

public class ProductsRepository implements ProductsMVP.Model {

    private static final String TAG = "ProductsRepository";

    private DatabaseReference myRef = null;
    public Integer idCounterListproducts;
    public Integer idCounterOrder;
    public Integer idCounterProducts;
    public Integer totalorder;
    public Integer preciounit;
    List<AddProductsInfo> list;
    private FirebaseAuth mAuth;

    private ProductsPresenter addOrdersPresenter;


    public ProductsRepository(Context context, ProductsPresenter addOrdersPresenter) {
        this.addOrdersPresenter = addOrdersPresenter;
        mAuth = FirebaseAuth.getInstance();
        myRef = FirebaseDatabase.getInstance().getReference();
        idCounterProducts=0;
        getFirstIdCounterListProducts();
        getIdCounterOrder();
        totalorder=0;
        preciounit=0;
        List<AddProductsInfo> list = new ArrayList<>();
        this.list=list;
        loadListProduct();
    }

    private void getFirstIdCounterListProducts() {
        getIdCounterListProducts(1);
    }

    private void getIdCounterOrder() {

        String correo = mAuth.getCurrentUser().getUid();
        myRef.child(correo).child("orderCounter").get()
                .addOnCompleteListener(new
                                               OnCompleteListener<DataSnapshot>() {
                                                   @Override
                                                   public void onComplete(@NonNull Task<DataSnapshot> task) {
                                                       if (!task.isSuccessful()) {
                                                           Log.e(TAG, "Error recibiendo datos",
                                                                   task.getException());
                                                       } else {
                                                           idCounterOrder =
                                                                   task.getResult().getValue(Integer.class);

                                                       }
                                                   }
                                               });
    }

    private void getIdCounterListProducts(Integer var) {

        String correo = mAuth.getCurrentUser().getUid();
        myRef.child(correo).child("listproductCounter").get()
                .addOnCompleteListener(new
                                               OnCompleteListener<DataSnapshot>() {
                                                   @Override
                                                   public void onComplete(@NonNull Task<DataSnapshot> task) {
                                                       if (!task.isSuccessful()) {
                                                           Log.e(TAG, "Error recibiendo datos",
                                                                   task.getException());
                                                       } else {
                                                           Log.d("TAG", "entro a getid" +task.getResult().getValue(Integer.class));
                                                           idCounterListproducts =
                                                                   task.getResult().getValue(Integer.class);
                                                           idCounterListproducts = idCounterListproducts+var;
                                                           saveIdListProductCounter();

                                                       }
                                                   }
                                               });
    }

    private void saveIdListProductCounter(){
        String correo = mAuth.getCurrentUser().getUid();
        myRef.child(correo).child("listproductCounter").setValue(idCounterListproducts);}

    private void saveIdOrderCounter(){
        String correo = mAuth.getCurrentUser().getUid();
        myRef.child(correo).child("orderCounter").setValue(idCounterOrder);
    }

    @Override
    public void loadProducts() {

     //   getIdCounterListProducts();
     //   Log.d("TAG", idCounterListproducts.toString());
        totalorder=0;
        String correo = mAuth.getCurrentUser().getUid();
        myRef.child(correo).child("products").get()
                .addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DataSnapshot> task) {
                        if (!task.isSuccessful()) {
                            Log.e(TAG, "Error recibiendo datos", task.getException());
                        } else {
                            List<ListProductsInfo> response = new ArrayList<>();

                            Log.d("TAG", String.valueOf(task.getResult().getValue()));
                            Log.d("TAG", "Trae estos elementos: " + task.getResult().getChildrenCount());
                            for (DataSnapshot snapshot : task.getResult().getChildren()) {
                                Log.d("TAG", "entro en el for1: ");
                                Log.d("TAG", "Elemento: " + snapshot);
                                Log.d("TAG", "la key es: " + snapshot.getKey());
                                if (snapshot.getKey().trim().equals(idCounterListproducts.toString().trim())){
                                    totalorder=0;
                                //for (DataSnapshot snap2 : snapshot.child(idCounterListproducts.toString()).getChildren()) {
                                    Log.d("TAG", "elemento snapshot: " + snapshot);

                                    for (DataSnapshot snap2 : snapshot.getChildren()) {
                                        Log.d("TAG", "elemento snap2: " + snap2);
                                        Log.d("TAG", "entro en el for2: ");
                                        ListProduct listProduct = snap2.getValue(ListProduct.class);
                                        if (listProduct != null) {
                                            Log.d("TAG", "Products: " + listProduct);

                                            Integer preciounit = getPrecioUnit(listProduct.getProduct());
                                            Log.d("TAG", "VAR precio unit: " + preciounit);
                                            Integer cant = Integer.valueOf(listProduct.getQuantity());
                                            Log.d("TAG", "VAR cant: " + cant);
                                            Integer total = preciounit * cant;
                                            Log.d("TAG", "VAR total prodc: " + total);
                                            Log.d("TAG", "VAR total antes de sumar: " + totalorder);
                                            totalorder=totalorder+total;
                                            Log.d("TAG", "VAR total despues de sumar: " + totalorder);
                                            response.add(new ListProductsInfo(listProduct.getProduct().toString().trim(), Integer.valueOf(listProduct.getQuantity().trim()),preciounit,total));
                                        }
                                    }

                                }

                            }
                            if (!response.isEmpty()) {
                                addOrdersPresenter.loadProducts(response);
                                addOrdersPresenter.loadTotal(totalorder);
                            }
                        }
                    }
                });
    }

    @Override
    public Integer getPrecioUnit(String product) {

        //Lista de productos

        for (AddProductsInfo data : this.list) {

            if (data.getProduct().trim().equals(product)){
                return Integer.valueOf(data.getQuantity());
            }
        }
        return 0;
    }

    @Override
    public void saveNewOrder(AddOrderInfo info) {
        getIdCounterOrder();
        Order order = new Order();
        order.setUid(idCounterOrder++);
        order.setClient(info.getClient());
        order.setListproduct(idCounterListproducts.toString());
        order.setShared(false);
        //todo validacion de tipo de dato entero
        order.setTotal(info.getTotalorder());

        String correo = mAuth.getCurrentUser().getUid();
        myRef.child(correo).child("orders").child(order.getUid().toString()).setValue(order);
        idCounterListproducts++;
        saveIdListProductCounter();
        saveIdOrderCounter();
    }

    @Override
    public void saveNewProduct(AddProductsInfo info) {
        //getIdCounterListProducts();
        ListProduct listProduct = new ListProduct();
        listProduct.setUid(idCounterProducts++);
        listProduct.setProduct(info.getProduct());
        listProduct.setQuantity(info.getQuantity().toString());

        String correo = mAuth.getCurrentUser().getUid();
        myRef.child(correo).child("products").child(idCounterListproducts.toString()).child(listProduct.getUid().toString()).setValue(listProduct);
        addOrdersPresenter.loadProducts();
    }

    @Override
    public Integer calculateTotal() {

    /*        Integer total=0;

            myRef.child("products").child(idCounterListproducts.toString()).get()
                    .addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DataSnapshot> task) {
                            if (!task.isSuccessful()) {
                                Log.e(TAG, "Error recibiendo datos",
                                        task.getException());
                            } else {
                                List<OrderInfo> response = new ArrayList<>();

                                for(DataSnapshot snapshot : task.getResult().getChildren()) {
                                    Log.d("TAG", "Elemento: "+snapshot);
                                    ListProduct product = snapshot.getValue(ListProduct.class);
                                    Log.d("TAG", "Order: "+product);

                                }
                                ordersPresenter.loadOrders(response);
                            }
                        }
                    });

*/
        return 0;
    }

    @Override
    public Integer getListProducts() {
        getIdCounterListProducts(0);
        return idCounterListproducts;
    }

    @Override
    public void loadProducts(String listproducts) {
        totalorder=0;
        String correo = mAuth.getCurrentUser().getUid();
        myRef.child(correo).child("products").get()
                .addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DataSnapshot> task) {
                        if (!task.isSuccessful()) {
                            Log.e(TAG, "Error recibiendo datos", task.getException());
                        } else {
                            List<ListProductsInfo> response = new ArrayList<>();

                            Log.d("TAG", String.valueOf(task.getResult().getValue()));
                            Log.d("TAG", "Trae estos elementos: " + task.getResult().getChildrenCount());
                            for (DataSnapshot snapshot : task.getResult().getChildren()) {
                                Log.d("TAG", "entro en el for1: ");
                                Log.d("TAG", "Elemento: " + snapshot);
                                Log.d("TAG", "la key es: " + snapshot.getKey());
                                if (snapshot.getKey().trim().equals(listproducts.trim())){
                                    totalorder=0;
                                    //for (DataSnapshot snap2 : snapshot.child(idCounterListproducts.toString()).getChildren()) {
                                    Log.d("TAG", "elemento snapshot: " + snapshot);

                                    for (DataSnapshot snap2 : snapshot.getChildren()) {
                                        Log.d("TAG", "elemento snap2: " + snap2);
                                        Log.d("TAG", "entro en el for2: ");
                                        ListProduct listProduct = snap2.getValue(ListProduct.class);
                                        if (listProduct != null) {
                                            Log.d("TAG", "Products: " + listProduct);

                                            Integer preciounit = getPrecioUnit(listProduct.getProduct());
                                            Log.d("TAG", "VAR precio unit: " + preciounit);
                                            Integer cant = Integer.valueOf(listProduct.getQuantity());
                                            Log.d("TAG", "VAR cant: " + cant);
                                            Integer total = preciounit * cant;
                                            Log.d("TAG", "VAR total prodc: " + total);
                                            Log.d("TAG", "VAR total antes de sumar: " + totalorder);
                                            totalorder=totalorder+total;
                                            Log.d("TAG", "VAR total despues de sumar: " + totalorder);
                                            response.add(new ListProductsInfo(listProduct.getProduct().toString().trim(), Integer.valueOf(listProduct.getQuantity().trim()),preciounit,total));
                                        }
                                    }

                                }

                            }
                            if (!response.isEmpty()==true) {
                                addOrdersPresenter.loadProducts(response);
                                addOrdersPresenter.loadTotal(totalorder);
                            }
                        }
                    }
                });
    }

    @Override
    public void loadClients() {

        String correo = mAuth.getCurrentUser().getUid();
        myRef.child(correo).child("client").get()
                .addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DataSnapshot> task) {
                        if (!task.isSuccessful()) {
                            Log.e(TAG, "Error recibiendo datos", task.getException());
                        } else {
                            try {
                                String [] response = new String[(int)task.getResult().getChildrenCount()];
                                int i=0;
                                Log.d("TAG", String.valueOf(task.getResult().getValue()));
                                Log.d("TAG", "Trae estos elementos: " + task.getResult().getChildrenCount());
                                for (DataSnapshot snapshot : task.getResult().getChildren()) {
                                    Log.d("TAG", "Elemento: " + snapshot);

                                    Client client = snapshot.getValue(Client.class);

                                    if (client != null) {
                                        Log.d("TAG", "Clients: " + client);

                                        response[i]=client.getName();
                                    }
                                    i++;
                                    Log.d("TAG", "Clients en response: ");
                                }
                                addOrdersPresenter.loadClients(response);
                            }catch (Exception e){Log.d("TAG", "Entra en el catch");}
                            }
                    }
                });
    }

    @Override
    public void loadListProduct() {

        myRef.child("product").get()
                .addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DataSnapshot> task) {
                        if (!task.isSuccessful()) {
                            Log.e(TAG, "Error recibiendo datos", task.getException());
                        } else {
                            try {
                                String [] response = new String[(int)task.getResult().getChildrenCount()];
                                int i=0;
                                Log.d("TAG", String.valueOf(task.getResult().getValue()));
                                Log.d("TAG", "Trae estos elementos: " + task.getResult().getChildrenCount());
                                for (DataSnapshot snapshot : task.getResult().getChildren()) {
                                    Log.d("TAG", "Elemento: " + snapshot);

                                    Product product = snapshot.getValue(Product.class);

                                    if (product != null) {
                                        Log.d("TAG", "Products: " + product);

                                        response[i]=product.getName();
                                        list.add(new AddProductsInfo(product.getName(), String.valueOf(product.getPreciounit())));
                                    }
                                    i++;
                                }
                                addOrdersPresenter.loadToTextProducts(response);
                            }catch (Exception e){Log.d("TAG", "Entra en el catch");}
                        }
                    }
                });
    }


}
