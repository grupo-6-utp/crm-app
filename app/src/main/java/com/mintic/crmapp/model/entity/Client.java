package com.mintic.crmapp.model.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Client {

    @PrimaryKey
    private Integer uid;

    private String name;

    private String direction;

    private String cel;

    private Integer nit;

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getCel() {
        return cel;
    }

    public void setCel(String cel) {
        this.cel = cel;
    }

    public Integer getNit() {
        return nit;
    }

    public void setNit(Integer nit) {
        this.nit = nit;
    }

    @Override
    public String toString(){
        return "Client{" +
                "uid=" + uid +
                ", name='" + name + '\'' +
                ", direction='" + direction + '\'' +
                ", cel='" + cel + '\'' +
                ", nit='" + nit +
                '}';
    }

}

