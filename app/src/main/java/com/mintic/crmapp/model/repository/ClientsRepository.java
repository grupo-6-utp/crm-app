package com.mintic.crmapp.model.repository;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mintic.crmapp.model.entity.Client;
import com.mintic.crmapp.model.entity.Order;
import com.mintic.crmapp.view.clients.ClientsInfo;
import com.mintic.crmapp.view.clients.ClientsMVP;
import com.mintic.crmapp.view.clients.ClientsPresenter;
import com.mintic.crmapp.view.orders.OrderInfo;

import java.util.ArrayList;
import java.util.List;

public class ClientsRepository implements ClientsMVP.Model {

    private static final String TAG = "CLientsRepository";
    private DatabaseReference myRef = null;
    private ClientsPresenter presenter;
    public Integer idCounterClient;
    private FirebaseAuth mAuth;

    public ClientsRepository(Context applicationContext, ClientsPresenter clientsPresenter) {
        this.presenter = clientsPresenter;
        mAuth = FirebaseAuth.getInstance();
        myRef = FirebaseDatabase.getInstance().getReference();
        getIdClient();
    }

    @Override
    public void loadClients() {
        String correo = mAuth.getCurrentUser().getUid();
        myRef.child(correo).child("client").get()
                .addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DataSnapshot> task) {
                        if (!task.isSuccessful()) {
                            Log.e(TAG, "Error recibiendo datos",
                                    task.getException());
                        } else {
                            List<ClientsInfo> response = new ArrayList<>();
                            Log.d("TAG", String.valueOf(task.getResult().getValue()));
                            Log.d("TAG", "Trae estos elementos: " + task.getResult().getChildrenCount());
                            for (DataSnapshot snapshot : task.getResult().getChildren()) {
                                Log.d("TAG", "Elemento: " + snapshot);
                                Client client = snapshot.getValue(Client.class);
                                Log.d("TAG", "Client: " + client);
                                response.add(new ClientsInfo(client.getUid(),client.getName(),client.getDirection(),client.getCel(),client.getNit().toString()));
                            }
                            presenter.loadClients(response);
                        }
                    }
                });
    }

    @Override
    public void newClients(ClientsInfo info) {
        Client client = new Client();
        client.setUid(idCounterClient++);
        client.setName(info.getName());
        client.setCel(info.getCel());
        client.setDirection(info.getDirection());
        client.setNit(Integer.valueOf(info.getNit()));

        String correo = mAuth.getCurrentUser().getUid();
        myRef.child(correo).child("client").child(client.getUid().toString()).setValue(client);
        saveIdClientCounter();
    }

    @Override
    public void cerrarsesion() {
        mAuth.signOut();
    }

    private void getIdClient() {

        String correo = mAuth.getCurrentUser().getUid();
        myRef.child(correo).child("clientCounter").get()
                .addOnCompleteListener(new
                                               OnCompleteListener<DataSnapshot>() {
                                                   @Override
                                                   public void onComplete(@NonNull Task<DataSnapshot> task) {
                                                       if (!task.isSuccessful()) {
                                                           Log.e(TAG, "Error recibiendo datos",
                                                                   task.getException());
                                                       } else {
                                                           idCounterClient =
                                                                   task.getResult().getValue(Integer.class);
                                                           Log.e(TAG, "El id counter "+String.valueOf(idCounterClient));
                                                       }
                                                   }
                                               });
    }

    private void saveIdClientCounter(){
        String correo = mAuth.getCurrentUser().getUid();
        myRef.child(correo).child("clientCounter").setValue(idCounterClient);
    }
}
