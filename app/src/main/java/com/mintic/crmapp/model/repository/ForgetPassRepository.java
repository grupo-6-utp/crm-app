package com.mintic.crmapp.model.repository;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.mintic.crmapp.view.UserRegister.UserRegisterMVP;
import com.mintic.crmapp.view.forgetpassword.ForgetPassMVP;

public class ForgetPassRepository implements ForgetPassMVP.Model {

    private static final String TAG = ForgetPassRepository.class.getSimpleName();

    private ForgetPassMVP.Presenter presenter;

    private FirebaseAuth mAuth;

    public ForgetPassRepository() {mAuth= FirebaseAuth.getInstance();}

    @Override
    public void passwordReset(String email) {
        mAuth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Log.d(TAG, "Email sent.");
                    presenter.passResetOK();
                } else{
                    presenter.passResetFailure(task.getException().getMessage());
                }
            }
        });
    }

    @Override
    public void setPresenter(ForgetPassMVP.Presenter presenter) {
        this.presenter = presenter;
    }

}
