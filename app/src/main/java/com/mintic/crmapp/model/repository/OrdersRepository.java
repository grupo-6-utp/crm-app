package com.mintic.crmapp.model.repository;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mintic.crmapp.model.entity.ListProduct;
import com.mintic.crmapp.model.entity.Order;
import com.mintic.crmapp.view.orders.OrderInfo;
import com.mintic.crmapp.view.orders.OrdersMVP;
import com.mintic.crmapp.view.orders.OrdersPresenter;
import com.mintic.crmapp.view.products.ListProductsInfo;

import java.util.ArrayList;
import java.util.List;

public class OrdersRepository implements OrdersMVP.Model {

    private static final String TAG = "OrdersRepository";

    private DatabaseReference myRef = null;
    private DatabaseReference myRef2 = null;
    private Integer totalorders;
    public StringBuilder response;
    private FirebaseAuth mAuth;

    private OrdersPresenter presenter;

    public OrdersRepository(Context context, OrdersPresenter ordersPresenter) {
        this.presenter = ordersPresenter;
        mAuth = FirebaseAuth.getInstance();
        myRef = FirebaseDatabase.getInstance().getReference();
        myRef2 = FirebaseDatabase.getInstance().getReference();
        response = new StringBuilder();
    }

    @Override
    public void loadOrders() {

        String correo = mAuth.getCurrentUser().getUid();
        myRef.child(correo).child("orders").get()
                .addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DataSnapshot> task) {
                        if (!task.isSuccessful()) {
                            Log.e(TAG, "Error recibiendo datos",
                                    task.getException());
                        } else {
                            List<OrderInfo> response = new ArrayList<>();
                            totalorders = 0;
                            Log.d("TAG", String.valueOf(task.getResult().getValue()));
                            Log.d("TAG", "Trae estos elementos: " + task.getResult().getChildrenCount());
                            for (DataSnapshot snapshot : task.getResult().getChildren()) {
                                Log.d("TAG", "Elemento: " + snapshot);
                                Order order = snapshot.getValue(Order.class);
                                Log.d("TAG", "Order: " + order);
                                if (!order.getShared()) {
                                    //
                                    String strtotalorder = order.getTotal().trim();
                                    totalorders = totalorders + Integer.parseInt(strtotalorder);
                                    response.add(new OrderInfo(order.getClient().trim(), order.getTotal().toString(), order.getListproduct(), order.getUid(), order.getShared()));
                                    //
                                }
                            }
                            presenter.loadOrders(response);
                            presenter.loadTotalOrders(totalorders);
                        }
                    }
                });
    }

    @Override
    public void cerrarsesion() {
        FirebaseAuth mAuth;
        mAuth = FirebaseAuth.getInstance();
        mAuth.signOut();
    }

    @Override
    public void loadProducts() {
        String correo = mAuth.getCurrentUser().getUid();
        myRef.child(correo).child("products").get()
                .addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DataSnapshot> task) {
                        if (!task.isSuccessful()) {
                            Log.e(TAG, "Error recibiendo datos", task.getException());
                        } else {
                            List<ListProductsInfo> response = new ArrayList<>();

                            Log.d("TAG", String.valueOf(task.getResult().getValue()));
                            Log.d("TAG", "Trae estos elementos: " + task.getResult().getChildrenCount());
                            for (DataSnapshot snapshot : task.getResult().getChildren()) {
                                Log.d("TAG", "entro en el for1: ");
                                Log.d("TAG", "Elemento: " + snapshot);
                                Log.d("TAG", "la key es: " + snapshot.getKey());
                                                                    //for (DataSnapshot snap2 : snapshot.child(idCounterListproducts.toString()).getChildren()) {
                                    Log.d("TAG", "elemento snapshot: " + snapshot);

                                    //



                                    //
                                    String key = snapshot.getKey();

                                    for (DataSnapshot snap2 : snapshot.getChildren()) {
                                        Log.d("TAG", "elemento snap2: " + snap2);
                                        Log.d("TAG", "entro en el for2: ");
                                        ListProduct listProduct = snap2.getValue(ListProduct.class);
                                        if (listProduct != null) {
                                            Log.d("TAG", "Products: " + listProduct);
                                            response.add(new ListProductsInfo(listProduct.getProduct().toString().trim(), Integer.valueOf(listProduct.getQuantity().trim()),Integer.valueOf(key),0));
                                        }
                                    }


                            }
                            if (!response.isEmpty()) {
                                presenter.loadProducts(response);
                            }
                        }
                    }
                });
    }

    @Override
    public void setShared(List<OrderInfo> ordersShare, Boolean var) {
        String correo = mAuth.getCurrentUser().getUid();
        for (OrderInfo info : ordersShare){
            myRef.child(correo).child("orders").child(info.getUid().toString()).child("shared").setValue(var);
        }
    }

}
