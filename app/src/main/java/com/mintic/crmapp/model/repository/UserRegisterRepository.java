package com.mintic.crmapp.model.repository;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mintic.crmapp.model.entity.User;
import com.mintic.crmapp.view.UserRegister.UserRegisterMVP;
import com.mintic.crmapp.view.login.MainMVP;

import java.util.List;


public class UserRegisterRepository implements UserRegisterMVP.Model {

    private static final String TAG = UserRegisterRepository.class.getSimpleName();

    private UserRegisterMVP.Presenter userRegisterPresenter;

    private FirebaseAuth mAuth;
    private DatabaseReference myRef = null;

    public UserRegisterRepository() {
        mAuth = FirebaseAuth.getInstance();
        myRef = FirebaseDatabase.getInstance().getReference();
//        this.myRef=myRef;
    }

    @Override
    public void setUserRegisterPresenter(UserRegisterMVP.Presenter userregpresenter) {
        this.userRegisterPresenter = userregpresenter;
    }

    @Override
    public void AddUser(String email, String password) {
        mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    userRegisterPresenter.AddUserSuccessful();
                } else {
                    userRegisterPresenter.AddUserFailure(task.getException().getMessage());
                }
            }
        });
    }

    @Override
    public void cerrarsesion() {
        FirebaseAuth mAuth;
        mAuth=FirebaseAuth.getInstance();
        String correo = mAuth.getCurrentUser().getUid();
        myRef.child(correo).child("clientCounter").setValue(0);
        myRef.child(correo).child("listproductCounter").setValue(0);
        myRef.child(correo).child("orderCounter").setValue(0);
        mAuth.signOut();
    }

}
