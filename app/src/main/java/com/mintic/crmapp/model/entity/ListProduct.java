package com.mintic.crmapp.model.entity;


import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class ListProduct {

    @PrimaryKey
    private Integer uid;

    private String product;

    private String quantity;

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString(){
        return "Products{" +
                "uid=" + uid +
                ", product='" + product + '\'' +
                ", quantity='" + quantity +
                '}';
    }

}
