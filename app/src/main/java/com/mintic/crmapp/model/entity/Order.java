package com.mintic.crmapp.model.entity;


import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Order {

    @PrimaryKey
    private Integer uid;

    private String client;

    private String listproduct;

    public Boolean getShared() {
        return shared;
    }

    public void setShared(Boolean shared) {
        this.shared = shared;
    }

    private Boolean shared;

    private String total;

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getListproduct() {
        return listproduct;
    }

    public void setListproduct(String listproduct) {
        this.listproduct = listproduct;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    @Override
    public String toString(){
        return "Order{" +
                "uid=" + uid +
                ", client='" + client + '\'' +
                ", listproduct='" + listproduct + '\'' +
                ", shared='" + shared + '\'' +
                ", total='" + total +
                '}';
    }

}
